import { IBaseRequest } from "../base/interface";

export interface IRedeemParams extends IBaseRequest {
  permit_value: String;
  permit_deadline: String;
  numTokens: String;
  minCollateral: String;
  feePercentage: String;
  recipient: String;
}
