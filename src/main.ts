import { ethers } from "ethers";
import { wei, weiToTokenAmount } from "./web3-helper/big_number";
import { FPN } from "./web3-helper/fixed_point_number";
import FORWARD_ABI from "./web3-helper/forwarder_abi.json";
import JARVIS_TOKEN_ABI from "./web3-helper/jarvis_token_abi.json";
import UNI_SWAP_ROUTER from "./web3-helper/uniswap_router.json";
const Web3 = require("web3");
const wait = (ms: number) => {
  console.log("Adding delay for ", ms);
  return new Promise((resolve) => setTimeout(resolve, ms));
};
const express = require("express");
const cors = require("cors");
const main = [];
const app = express();
const port = 3000;
const relayer = "0xD10C1BD6C7dF0C03743145bDCE923Ff415fF8915";
const provider = new ethers.providers.JsonRpcProvider(
  "https://speedy-nodes-nyc.moralis.io/4007920be6f32643c009ddd0/polygon/mumbai"
);
const web3 = new Web3("https://rpc-mumbai.maticvigil.com");
const privateKey =
  "0xcd52656f55d3230a45c793c330183e26bc2867682aeeeac71cd0e2b20fa39cda";
web3.eth.accounts.privateKeyToAccount(privateKey);
web3.eth.accounts.wallet.add(privateKey);
web3.eth.defaultAccount = (privateKey as any).address;
const permit = async ({ tokenContract, requestData, to, gasPrice }) => {
  /* -------------------------------------------------------------------------- */
  /*                         Perform Permit Transaction                         */
  /* -------------------------------------------------------------------------- */

  const _allowanceAllowed = FPN.fromWei(
    await tokenContract.allowance(requestData.from, to)
  );
  console.log(
    tokenContract.address,
    requestData.from,
    to,
    (await tokenContract.allowance(requestData.from, to)).toString()
  );
  const tokenDecimals = await tokenContract.decimals();

  const allowanceAllowed = weiToTokenAmount({
    wei: wei(_allowanceAllowed.toString()),
    decimals: tokenDecimals,
  });

  const isSufficientAllowance = allowanceAllowed.gte(
    FPN.fromWei(requestData.value).bn
  );
  console.log(
    `Permit Allowance Sufficient: ${isSufficientAllowance} \n -> MaxAllowance: ${weiToTokenAmount(
      {
        wei: wei(requestData.allowance_Value),
        decimals: tokenDecimals,
      }
    ).toString()} \n -> Allowed: ${allowanceAllowed.toString()} \n -> Amount : ${FPN.fromWei(
      requestData.value
    ).toString()}
        `
  );
  if (!isSufficientAllowance) {
    const amount = FPN.fromWei(requestData.value);
    if (amount.bn.gt(allowanceAllowed)) {
      console.log({ message: "Performing Permit Transaction" });
      console.log([
        requestData.from.toLowerCase(),
        to.toLowerCase(),
        requestData.allowance_Value,
        requestData.deadline,
        requestData.v,
        requestData.r,
        requestData.s,
      ]);
      const estimatePermitTxGas = await tokenContract.estimateGas.permit(
        requestData.from.toLowerCase(),
        to.toLowerCase(),
        requestData.allowance_Value,
        requestData.deadline,
        requestData.v,
        requestData.r,
        requestData.s
      );
      console.log({
        msg: "Estimated Permit Gas Cost",
        gas: estimatePermitTxGas.toString(),
      });

      const permitTx = await tokenContract.permit(
        requestData.from.toLowerCase(),
        to.toLowerCase(),
        requestData.allowance_Value,
        requestData.deadline,
        requestData.v,
        requestData.r,
        requestData.s,
        {
          gasPrice: gasPrice,
          gasLimit: estimatePermitTxGas,
        }
      );
      // main.push(permitTx.data);
      //  console.log({ permitTx });
      await permitTx.wait();
      let isAllowanceIncrease = "0";
      do {
        isAllowanceIncrease = await tokenContract.allowance(
          requestData.from,
          to
        );
        console.log("Waiting for tx to broadcast");
        await wait(4000);
      } while (isAllowanceIncrease.toString() === "0");

      await wait(10000);
    } else {
      console.log({
        message: "Amount less than allowance no permit tx required ",
      });
    }
  }
};
app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  next();
});
app.get("/", (req, res) => {
  res.send("Hello World!");
});
// `app.use(express.json())` **before** your route handlers!
app.use(express.json());
const signer = new ethers.Wallet(privateKey, provider);
const forwardContract_v6 = new ethers.Contract(
  "0x2e45a0a633472e5879b15bf3af78f156909103cc",
  FORWARD_ABI,
  signer
);
const uniSwapRouter = new ethers.Contract(
  "0x68b3465833fb72A70ecDF485E0e4C7bD8665Fc45",
  UNI_SWAP_ROUTER,
  signer
);

const uniSwapRouterWeb3 = new web3.eth.Contract(
  UNI_SWAP_ROUTER,
  "0x68b3465833fb72A70ecDF485E0e4C7bD8665Fc45"
);
app.post("*", async (req, res) => {
  req.body; // JavaScript object containing the parse JSON
  const data = req.body;
  const tokenContract = new ethers.Contract(data.to, JARVIS_TOKEN_ABI, signer);
  const gasPrice = await provider.getGasPrice();

  await permit({
    tokenContract,
    requestData: data,
    to: data.pool,
    gasPrice,
  });
  const forwarderNonce = await forwardContract_v6.getNonce(data.from);
  console.log([
    data.from,
    data.pool,
    ethers.utils.parseEther("0"),
    data.redeemGas,
    parseInt(forwarderNonce.toString()),
    data.data,
  ]);

  const tx2 = await forwardContract_v6.safeExecute(
    [
      data.from,
      data.pool,
      ethers.utils.parseEther("0"),
      data.redeemGas,
      parseInt(forwarderNonce.toString()),
      data.data,
    ],
    data.signature
  );
  // main.push(tx2.data);
  console.log({ main });
  //   const x = await uniSwapRouter.populateTransaction.multicall(main);
  //   console.log(x);
  //   console.log(
  //     uniSwapRouter.multicall,
  //     uniSwapRouterWeb3.methods.multicall(1655907491, main).encodeABI()
  //   );
  //   console.log(
  //     await uniSwapRouterWeb3.methods
  //       .multicall(main)
  //       .send({ from: relayer, gas: 1000000, value: 0 })
  //   );
  //   console.log({
  //     msg: "Estimated Permit Gas Cost",
  //     gas: x.toString(),
  //   });
  console.log(await tx2.wait());
  res.json(req.body);
});
app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});
