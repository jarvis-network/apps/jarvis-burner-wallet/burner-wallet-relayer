import BN from "bn.js";
import { fromWei, toWei, Unit } from "web3-utils";
export function mapReduce<T, U, R>(
  array: readonly T[],
  initialValue: R,
  mapFn: (elem: T) => U,
  reduceFn: (curr: R, next: U) => R
) {
  return array.reduce(
    (curr, next) => reduceFn(curr, mapFn(next)),
    initialValue
  );
}

type Empty = null | undefined | never;
type Tag<Kind, Data = null> = Data extends null
  ? { kind: Kind }
  : {
      kind: Kind;
      data: Data;
    };

type TaggedValue<Type, Kind, Data> = Type & Tag<Kind, Data>;
// eslint-disable-next-line @typescript-eslint/ban-types
export type Id<T> = {} & { [K in keyof T]: T[K] };

export type Tagged<Type, Kind> = Type extends TaggedValue<
  infer BaseType,
  infer BaseKind,
  infer BaseData
>
  ? BaseData extends Empty
    ? TaggedValue<BaseType, BaseKind, Kind>
    : TaggedValue<BaseType, BaseKind, Id<Kind & BaseData>>
  : TaggedValue<Type, Kind, null>;

export type Obj = Record<string | number, unknown>;

type AssertFunc = (value: any, message?: string) => asserts value;

export const assert: AssertFunc =
  process.env.app_env !== "browser"
    ? // eslint-disable-next-line @typescript-eslint/no-var-requires
      require("assert").strict
    : (value: any, message?: string) => {
        if (!value) throw new Error(message);
      };

export function isString(x: unknown): x is string {
  return typeof x === "string";
}
export function isInteger(x: unknown): x is number {
  return Number.isInteger(x);
}

export function assertIsInteger(x: unknown): number {
  assert(isInteger(x), `value=${x} is not a number.`);
  return x;
}

export function isNumericString(str: unknown): boolean {
  return typeof str === "string" && isFinite(parseFloat(str));
}

export function assertIsNumericString(x: string): string {
  assert(isNumericString(x));
  return x;
}
export function isObject(x: unknown): x is Obj {
  return typeof x === "object" && x !== null;
}
export const ether = new BN(10).pow(new BN(18));
export const zero = new BN(0);
export const one = new BN(1);
export const negativeOne = new BN(-1);
export const maxUint256 = new BN(
  "ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff",
  "hex"
);

export type AmountOf<U extends Unit> = Tagged<BN, U>;
export type Amount = AmountOf<"wei">;

export type StringAmountOf<U extends Unit> = Tagged<string, U>;
export type StringAmount = StringAmountOf<"wei">;

export function weiString(str: string): StringAmount {
  assertIsInteger(str);
  return str as StringAmount;
}

export function toWeiString(str: string): StringAmount {
  return toWei(assertIsNumericString(str)) as StringAmount;
}

export function wei(str: string | number): Amount {
  return new BN(str, 10) as Amount;
}

export function numberToWei(n: number): Amount {
  return new BN(toWei(n.toFixed(18))) as Amount;
}

export function scale(a: BN, b: number): BN {
  return a.mul(numberToWei(b)).div(ether);
}

export function mapSumBN<T, U extends Unit>(
  array: T[],
  getter: (elem: T) => AmountOf<U>
): AmountOf<U> {
  return mapReduce(
    array,
    zero as AmountOf<U>,
    getter,
    (curr, next) => curr.add(next) as AmountOf<U>
  );
}

export function sumBN<U extends Unit>(
  listOfNumbers: AmountOf<U>[]
): AmountOf<U> {
  return mapSumBN(listOfNumbers, (x) => x);
}

export function weiToNumber(weiStr: string): number {
  return fromBNToNumber(new BN(weiStr));
}

export function fromBNToDecimalString(bn: BN): string {
  return fromWei(bn);
}

export function formatAmount(amount: Amount, decimals = 2): string {
  const rawStr = amount.toString(10);
  const nativeNumDecimals = 18;
  const integerPart = rawStr.slice(0, -nativeNumDecimals).padStart(1, "0");
  const decimalPart = rawStr
    .slice(-nativeNumDecimals)
    .padStart(nativeNumDecimals, "0");
  return `${integerPart}.${decimalPart.slice(0, decimals)}`;
}

export function fromBNToNumber(bn: BN): number {
  return Number.parseFloat(fromBNToDecimalString(bn));
}

export function toBN(str: string): BN {
  return new BN(toWei(str.replace(/,/g, "")));
}

export function replaceBN(obj: unknown): unknown {
  if (BN.isBN(obj)) {
    return formatAmount(obj as Amount);
  }
  if (!isObject(obj)) {
    return obj;
  }
  const result: any = {};
  for (const [key, value] of Object.entries(obj)) {
    result[key] = replaceBN(value);
  }
  return result;
}
type WeiToTokenAmountParams = {
  wei: Amount;
  decimals: number;
};

export function weiToTokenAmount({
  wei,
  decimals,
}: WeiToTokenAmountParams): BN {
  const scaleFactor = new BN(10).pow(new BN(18 - decimals));
  return wei.div(scaleFactor);
}
