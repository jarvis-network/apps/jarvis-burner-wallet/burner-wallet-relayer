import { ethers } from "ethers";
import { FPN } from "../web3-helper/fixed_point_number";
const MAX_ALLOWANCE_IN_ETH = 1000000000000;

export const isSufficientAllowance = async (
  contractInstance: ethers.Contract,
  owner: String,
  spender: String,
  amount: FPN
) => {
  const allowanceAllowed = FPN.fromWei(
    await contractInstance.allowance(owner, spender)
  );
  return allowanceAllowed.gte(amount);
};

export const setApproveAllowance = async (
  contractInstance: ethers.Contract,
  gasPrice: ethers.BigNumber,
  spender: String
) => {
  /* -------------------------------------------------------------------------- */
  /*                             Approve Allowance                              */
  /* -------------------------------------------------------------------------- */
  const estimateApproveTxGas =
    await contractInstance.estimateGas.increaseAllowance(
      spender,
      MAX_ALLOWANCE_IN_ETH
    );

  const approveTx = await contractInstance.increaseAllowance(
    spender,
    MAX_ALLOWANCE_IN_ETH,
    {
      gasPrice: gasPrice,
      gasLimit: estimateApproveTxGas,
    }
  );
  console.log(approveTx);
  await approveTx.wait();
};

export const increaseAllowance = async (
  contractInstance: ethers.Contract,
  gasPrice: ethers.BigNumber,
  spender: String,
  amount: String
) => {
  /* -------------------------------------------------------------------------- */
  /*                             Increase Allowance                             */
  /* -------------------------------------------------------------------------- */
  const estimateIncreaseAllowanceTxGas =
    await contractInstance.estimateGas.increaseAllowance(spender, amount);
  console.log(estimateIncreaseAllowanceTxGas.toString());
  const increaseAllowanceTx = await contractInstance.increaseAllowance(
    spender,
    amount,
    {
      gasPrice: gasPrice,
      gasLimit: 1000000,
    }
  );
  console.log(increaseAllowanceTx);
  await increaseAllowanceTx.wait();
};
