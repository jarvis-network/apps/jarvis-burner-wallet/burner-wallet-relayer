export enum BASE_OPERATION {
  "REDEEM",
  "MINT",
  "EXCHANGE",
}

export interface IBaseRequest {
  op_type: keyof typeof BASE_OPERATION;
  from: String;
  to: String;
  pool: String;
  r: String;
  s: String;
  v: number;
}
