// Import dependencies available in the autotask environment

import {
  AuthenticationDetails,
  CognitoUser,
  CognitoUserPool,
} from "amazon-cognito-identity-js";
import axios from "axios";
import {
  DefenderRelayProvider,
  DefenderRelaySigner,
} from "defender-relay-client/lib/ethers";
import ethers from "ethers";
import _ from "underscore";
import ATOMIC_SWAP_ABI from "./web3-helper/atomic_swap.json";
import { wei, weiToTokenAmount } from "./web3-helper/big_number";
import JARVIS_BSC_POOL_ABI from "./web3-helper/bsc_pool_abi.json";
import BTC_TOKEN_ABI from "./web3-helper/btc_token_abi.json";
import BUSD_TOKEN_ABI from "./web3-helper/busd_token_abi.json";
import { FPN } from "./web3-helper/fixed_point_number";
import FORWARD_ABI from "./web3-helper/forwarder_abi.json";
import JARVIS_TOKEN_ABI from "./web3-helper/jarvis_token_abi.json";
import {
  default as JARVIS_POOL_ABI,
  default as JARVIS_USDC_POOL_ABI,
} from "./web3-helper/pool_abi.json";
import USDC_TOKEN_ABI from "./web3-helper/usdc_token_abi.json";
const MAX_ALLOWANCE_IN_ETH = 1000000000000;
const wait = (ms: number) => {
  console.log("Adding delay for ", ms);
  return new Promise((resolve) => setTimeout(resolve, ms));
};
const permit = async ({ tokenContract, requestData, to, gasPrice }) => {
  /* -------------------------------------------------------------------------- */
  /*                         Perform Permit Transaction                         */
  /* -------------------------------------------------------------------------- */

  const _allowanceAllowed = FPN.fromWei(
    await tokenContract.allowance(requestData.from, to)
  );
  console.log(
    tokenContract.address,
    requestData.from,
    to,
    (await tokenContract.allowance(requestData.from, to)).toString()
  );
  const tokenDecimals = await tokenContract.decimals();

  const allowanceAllowed = weiToTokenAmount({
    wei: wei(_allowanceAllowed.toString()),
    decimals: tokenDecimals,
  });

  const isSufficientAllowance = allowanceAllowed.gte(
    FPN.fromWei(requestData.value).bn
  );
  console.log(
    `Permit Allowance Sufficient: ${isSufficientAllowance} \n -> MaxAllowance: ${weiToTokenAmount(
      {
        wei: wei(requestData.allowance_Value),
        decimals: tokenDecimals,
      }
    ).toString()} \n -> Allowed: ${allowanceAllowed.toString()} \n -> Amount : ${FPN.fromWei(
      requestData.value
    ).toString()}
      `
  );
  if (!isSufficientAllowance) {
    const amount = FPN.fromWei(requestData.value);
    if (amount.bn.gt(allowanceAllowed)) {
      console.log({ message: "Performing Permit Transaction" });
      console.log([
        requestData.from.toLowerCase(),
        to.toLowerCase(),
        requestData.allowance_Value,
        requestData.deadline,
        requestData.v,
        requestData.r,
        requestData.s,
      ]);
      const estimatePermitTxGas = await tokenContract.estimateGas.permit(
        requestData.from.toLowerCase(),
        to.toLowerCase(),
        requestData.allowance_Value,
        requestData.deadline,
        requestData.v,
        requestData.r,
        requestData.s
      );
      console.log({
        msg: "Estimated Permit Gas Cost",
        gas: estimatePermitTxGas.toString(),
      });

      const permitTx = await tokenContract.permit(
        requestData.from.toLowerCase(),
        to.toLowerCase(),
        requestData.allowance_Value,
        requestData.deadline,
        requestData.v,
        requestData.r,
        requestData.s,
        {
          gasPrice: gasPrice,
          gasLimit: estimatePermitTxGas,
        }
      );
      console.log(permitTx);
      await permitTx.wait();
      let isAllowanceIncrease = "0";
      do {
        isAllowanceIncrease = await tokenContract.allowance(
          requestData.from,
          to
        );
        console.log("Waiting for tx to broadcast");
        await wait(4000);
      } while (isAllowanceIncrease.toString() === "0");

      // await wait(10000);
    } else {
      console.log({
        message: "Amount less than allowance no permit tx required ",
      });
    }
  }
};

const poolSynthApprove = async ({
  tokenContract,
  requestData,
  relayerAddress,
  gasPrice,
}) => {
  const tokenDecimals = await tokenContract.decimals();
  const _allowanceAllowed = FPN.fromWei(
    await tokenContract.allowance(relayerAddress, requestData.pool)
  );
  const allowanceAllowed = weiToTokenAmount({
    wei: wei(_allowanceAllowed.toString()),
    decimals: tokenDecimals,
  });

  const isSufficientAllowance = allowanceAllowed.gte(
    weiToTokenAmount({
      wei: wei(requestData.value),
      decimals: tokenDecimals,
    })
  );

  console.log(
    `Synth Pool Allowance Sufficient: ${isSufficientAllowance} \n -> MaxAllowance: ${weiToTokenAmount(
      {
        wei: wei(requestData.allowance_Value),
        decimals: tokenDecimals,
      }
    ).toString()} \n -> Allowed: ${allowanceAllowed.toString()} \n -> Amount : ${FPN.fromWei(
      requestData.value
    ).toString()}
      `
  );

  if (!isSufficientAllowance) {
    /* -------------------------------------------------------------------------- */
    /*                                 Approve TX                                 */
    /* -------------------------------------------------------------------------- */

    const estimateApproveTxGas = await tokenContract.estimateGas.approve(
      requestData.pool,
      new FPN(MAX_ALLOWANCE_IN_ETH).toString()
    );
    console.log({
      msg: "estimateApproveTxGas for pool approve",
      gas: estimateApproveTxGas.toString(),
    });
    const approveTx = await tokenContract.approve(
      requestData.pool,
      new FPN(MAX_ALLOWANCE_IN_ETH).toString(),
      {
        gasPrice: gasPrice,
        gasLimit: estimateApproveTxGas,
      }
    );
    console.log(approveTx);
    await approveTx.wait();
    // await wait(10000);
  }
};

const synthTransfer = async ({
  tokenContract,
  requestData,
  from,
  to,
  gasPrice,
}) => {
  /* -------------------------------------------------------------------------- */
  /*                                Transfer From                               */
  /* -------------------------------------------------------------------------- */

  console.log({ msg: `Transferring Fund to Relayer` });
  const estimateTransferTx = await tokenContract.estimateGas.transferFrom(
    from,
    to,
    requestData.value
  );

  const transferUsd = await tokenContract.transferFrom(
    from,
    to,
    requestData.value,
    {
      gasPrice: gasPrice,
      gasLimit: estimateTransferTx,
    }
  );
  console.log(transferUsd);
  await transferUsd.wait();
  // await wait(10000);
};

const revertSynthTransfer = async ({
  tokenContract,
  requestData,
  to,
  gasPrice,
}: {
  tokenContract: ethers.ethers.Contract;
  requestData: any;
  to: any;
  gasPrice: ethers.ethers.BigNumber;
}) => {
  /* -------------------------------------------------------------------------- */
  /*                                Transfer From                               */
  /* -------------------------------------------------------------------------- */

  console.log({
    msg: `Transferring Fund back from Relayer to User`,
    to,
    amount: requestData.value.toString(),
    token: tokenContract.address,
    from: await tokenContract.signer.getAddress(),
  });

  const estimateTransferTx = await tokenContract.estimateGas.transfer(
    to,
    requestData.value.toString()
  );

  const transferUsd = await tokenContract.transfer(
    to,
    requestData.value.toString(),
    {
      gasPrice: gasPrice,
      gasLimit: estimateTransferTx,
    }
  );
  console.log(transferUsd);
  await transferUsd.wait();
  // await wait(10000);
};

const redeem = async ({ poolContract, requestData, gasPrice }) => {
  console.log({ msg: `Performing Redeem Transaction` });
  /* -------------------------------------------------------------------------- */
  /*                                   Redeem                                   */
  /* -------------------------------------------------------------------------- */
  const derivative = await poolContract.getAllDerivatives();
  const numTokens = requestData.value;
  const minCollateral = new FPN(0).toString();
  const feePercentage = new FPN(0.001).toString();
  const expiration = requestData.deadline;
  const recipient = requestData.from;
  console.log({
    derivative: derivative[0],
    numTokens,
    minCollateral,
    feePercentage,
    expiration,
    recipient,
  });

  const estimateRedeemTx = await poolContract.estimateGas.redeem([
    derivative[0],
    numTokens,
    minCollateral,
    feePercentage,
    expiration,
    recipient,
  ]);
  const redeemTx = await poolContract.redeem(
    [
      derivative[0],
      numTokens,
      minCollateral,
      feePercentage,
      expiration,
      recipient,
    ],
    {
      gasPrice: gasPrice,
      gasLimit: estimateRedeemTx,
    }
  );
  console.log(redeemTx);
  await redeemTx.wait();
  // await wait(10000);
};

const executeMetaTx = async ({
  tokenInstance,
  requestData,
  address,
  gasPrice,
}) => {
  const tokenDecimals = await tokenInstance.decimals();
  const _allowanceAllowed = FPN.fromWei(
    await tokenInstance.allowance(requestData.from, address)
  );
  const allowanceAllowed = weiToTokenAmount({
    wei: wei(_allowanceAllowed.toString()),
    decimals: tokenDecimals,
  });

  const isSufficientAllowance = allowanceAllowed.gte(
    weiToTokenAmount({
      wei: wei(requestData.value),
      decimals: tokenDecimals,
    })
  );

  console.log(
    `Execute Allowance Sufficient: ${isSufficientAllowance} \n -> MaxAllowance: ${weiToTokenAmount(
      {
        wei: wei(requestData.allowance_Value),
        decimals: tokenDecimals,
      }
    ).toString()} \n -> Allowed: ${allowanceAllowed.toString()} \n -> Amount : ${FPN.fromWei(
      requestData.value
    ).toString()}
      `
  );
  if (!isSufficientAllowance) {
    const amount = FPN.fromWei(requestData.value);
    if (amount.bn.gt(allowanceAllowed)) {
      console.log({ message: "Performing Meta Tx Transaction" });
      const estimateMetaTxGas =
        await tokenInstance.estimateGas.executeMetaTransaction(
          requestData.from,
          requestData.functionSignature,
          requestData.r,
          requestData.s,
          requestData.v
        );
      console.log({
        msg: "Estimated MetaTx Gas Cost",
        gas: estimateMetaTxGas.toString(),
      });

      const metaTx = await tokenInstance.executeMetaTransaction(
        requestData.from,
        requestData.functionSignature,
        requestData.r,
        requestData.s,
        requestData.v,
        {
          gasPrice: gasPrice,
          gasLimit: estimateMetaTxGas,
        }
      );
      console.log(metaTx);
      await metaTx.wait();
      // await wait(10000);
    } else {
      console.log({
        message: "Amount less than allowance no approve tx required ",
      });
    }
  }
};
const approveToken = async ({
  tokenInstance,
  requestData,
  owner,
  spender,
  gasPrice,
}) => {
  const tokenDecimals = await tokenInstance.decimals();
  const _allowanceAllowed = FPN.fromWei(
    await tokenInstance.allowance(owner, spender)
  );
  let isSufficientAllowance = false;
  if (!_allowanceAllowed.eq(new FPN(0))) {
    const allowanceAllowed = weiToTokenAmount({
      wei: wei(_allowanceAllowed.toString()),
      decimals: tokenDecimals,
    });

    isSufficientAllowance = allowanceAllowed.gte(
      weiToTokenAmount({
        wei: wei(requestData.value),
        decimals: tokenDecimals,
      })
    );
    console.log(
      `Pool Allowance Sufficient: ${isSufficientAllowance} \n -> Required: ${weiToTokenAmount(
        {
          wei: wei(requestData.allowance_Value),
          decimals: tokenDecimals,
        }
      ).toString()} \n -> Allowed: ${_allowanceAllowed.toString()} \n -> Amount : ${FPN.fromWei(
        requestData.value
      ).toString()}`
    );
  }

  if (!isSufficientAllowance) {
    /* -------------------------------------------------------------------------- */
    /*                                 Approve TX                                 */
    /* -------------------------------------------------------------------------- */

    console.log({ message: "Performing Approve Transaction For Pool" });
    const estimateApproveTxGas = await tokenInstance.estimateGas.approve(
      spender,
      weiToTokenAmount({
        wei: wei(new FPN(MAX_ALLOWANCE_IN_ETH).toString()),
        decimals: tokenDecimals,
      }).toString()
    );
    console.log(
      {
        msg: "estimateApproveTxGas for pool approve",
        gas: estimateApproveTxGas.toString(),
      },
      [
        spender,
        weiToTokenAmount({
          wei: wei(new FPN(MAX_ALLOWANCE_IN_ETH).toString()),
          decimals: tokenDecimals,
        }).toString(),
        {
          gasPrice: gasPrice,
          gasLimit: estimateApproveTxGas,
        },
      ]
    );
    const approveTx = await tokenInstance.approve(
      spender,
      weiToTokenAmount({
        wei: wei(new FPN(MAX_ALLOWANCE_IN_ETH).toString()),
        decimals: tokenDecimals,
      }).toString(),
      {
        gasPrice: gasPrice,
        gasLimit: estimateApproveTxGas,
      }
    );
    console.log(approveTx);
    await approveTx.wait();
    // await wait(10000);
  }
};
const tokenTransfer = async ({
  tokenInstance,
  requestData,
  from,
  to,
  gasPrice,
}) => {
  console.log({ msg: `Transferring Fund to Relayer` });
  const estimateTransferTx = await tokenInstance.estimateGas.transferFrom(
    from,
    to,
    requestData.value
  );
  /* -------------------------------------------------------------------------- */
  /*                                Transfer From                               */
  /* -------------------------------------------------------------------------- */

  const transferUsd = await tokenInstance.transferFrom(
    from,
    to,
    requestData.value,
    {
      gasPrice: gasPrice,
      gasLimit: estimateTransferTx,
    }
  );
  console.log(transferUsd);
  await transferUsd.wait();
  // await wait(10000);
};
const mint = async ({ poolContract, requestData, gasPrice }) => {
  console.log({ msg: `Performing Mint Transaction` });

  const numTokens = new FPN(0).toString();
  const minCollateral = requestData.value;
  const feePercentage = new FPN(0.001).toString();
  const expiration = requestData.deadline;
  const recipient = requestData.from;
  const derivative = await poolContract.getAllDerivatives();

  console.log({
    derivative: derivative[0],
    numTokens,
    minCollateral,
    feePercentage,
    expiration,
    recipient,
  });

  const estimateMintTx = await poolContract.estimateGas.mint([
    derivative[0],
    numTokens,
    minCollateral,
    feePercentage,
    expiration,
    recipient,
  ]);
  const mintTx = await poolContract.mint(
    [
      derivative[0],
      numTokens,
      minCollateral,
      feePercentage,
      expiration,
      recipient,
    ],
    {
      gasPrice: gasPrice,
      gasLimit: estimateMintTx,
    }
  );
  console.log(mintTx);
  await mintTx.wait();
  // await wait(10000);
};
const exchange = async ({
  poolContract,
  requestData,
  signer,
  gasPrice,
}: {
  poolContract: ethers.ethers.Contract;
  requestData: any;
  signer: DefenderRelaySigner;
  gasPrice: ethers.ethers.BigNumber;
}) => {
  console.log({ msg: `Performing Exchange Transaction` });

  const poolDestContract = new ethers.Contract(
    requestData.destPool,
    JARVIS_POOL_ABI,
    signer
  );
  const minDestNumTokens = new FPN(0).toString();
  const numTokens = requestData.value;
  const feePercentage = new FPN(0.001).toString();
  const expiration = requestData.deadline;
  const recipient = requestData.from;
  const derivative = await poolContract.getAllDerivatives();
  const destDerivative = await poolDestContract.getAllDerivatives();

  console.log({
    derivative: derivative[0],
    destPool: requestData.destPool,
    destDerivative: destDerivative[0],
    numTokens,
    minDestNumTokens,
    feePercentage,
    expiration,
    recipient,
  });

  const estimateExchangeTx = await poolContract.estimateGas.exchange([
    derivative[0],
    requestData.destPool,
    destDerivative[0],
    numTokens,
    minDestNumTokens,
    feePercentage,
    expiration,
    recipient,
  ]);
  console.log({
    msg: "estimateExchangeTx for pool exchange",
    gas: estimateExchangeTx.toString(),
  });

  const exchangesTX = await poolContract.populateTransaction.exchange(
    [
      derivative[0],
      requestData.destPool,
      destDerivative[0],
      numTokens,
      minDestNumTokens,
      feePercentage,
      expiration,
      recipient,
    ],
    {
      gasPrice: gasPrice,
      gasLimit: estimateExchangeTx,
    }
  );
  console.log(exchangesTX);

  const exchangeTX = await poolContract.exchange(
    [
      derivative[0],
      requestData.destPool,
      destDerivative[0],
      numTokens,
      minDestNumTokens,
      feePercentage,
      expiration,
      recipient,
    ],
    {
      gasPrice: gasPrice,
      gasLimit: estimateExchangeTx.add(10000),
    }
  );
  console.log(exchangeTX);
  await exchangeTX.wait();
  // await wait(10000);
};

function createApi(key, token, apiUrl) {
  const instance = axios.create({
    baseURL: apiUrl,
    headers: {
      "X-Api-Key": key,
      Authorization: `Bearer ${token}`,
      "Content-Type": "application/json",
    },
  });
  instance.interceptors.response.use(
    (response) => response.data,
    (error) =>
      Promise.reject({
        response: _.pick(error.response, "status", "statusText", "data"),
        message: error.message,
        request: _.pick(error.request, "path", "method"),
      })
  );
  return instance;
}
// Entrypoint for the Autotask
export async function handler(event) {
  const requestData = event.request.body;
  const credentials = { ...event };

  const provider = new DefenderRelayProvider(credentials);

  const signer = new DefenderRelaySigner(credentials, provider, {
    speed: "fast",
  });

  if (requestData.op_type === "LOG") {
    const { LI_KEY: apiKey, LI_SECRET: apiSecret } = event.secrets;

    const authenticationDetails = new AuthenticationDetails({
      Username: apiKey,
      Password: apiSecret,
    });
    const userPool = new CognitoUserPool({
      UserPoolId: "us-west-2_94f3puJWv",
      ClientId: "40e58hbc7pktmnp9i26hh5nsav",
    });
    const userData = {
      Username: apiKey,
      Pool: userPool,
    };
    const cognitoUser = new CognitoUser(userData);
    const token = await new Promise((resolve, reject) => {
      cognitoUser.authenticateUser(authenticationDetails, {
        onSuccess: function (session) {
          //    console.log("session", session);
          const token = session.getAccessToken().getJwtToken();
          resolve(token);
        },
        onFailure: function (err) {
          console.error(`Failed to get a token for the API key abc`, err);
          reject(err);
        },
      });
    });
    const api = createApi(
      userData.Username,
      token,
      "https://defender-api.openzeppelin.com/autotask/"
    );
    const resp = await api.get(
      `/autotasks/runs/b6d94542-ee9e-4e08-90c7-ca7365090b95`
    );

    return resp;
  }

  const relayerAddress = await signer.getAddress();
  const gasPrice = await provider.getGasPrice();
  console.log({ requestData });
  console.log({
    before: (await provider.getGasPrice()).toString(),
    after: gasPrice.toString(),
  });
  const tokenContract = new ethers.Contract(
    requestData.to,
    JARVIS_TOKEN_ABI,
    signer
  );

  const poolContractBusd = requestData.pool
    ? new ethers.Contract(requestData.pool, JARVIS_BSC_POOL_ABI, signer)
    : new ethers.Contract(requestData.to, JARVIS_TOKEN_ABI, signer);

  const poolContractUsdc = requestData.pool
    ? new ethers.Contract(requestData.pool, JARVIS_USDC_POOL_ABI, signer)
    : new ethers.Contract(requestData.to, JARVIS_TOKEN_ABI, signer);

  const busdTokenContract = new ethers.Contract(
    "0xe9e7CEA3DedcA5984780Bafc599bD69ADd087D56",
    BUSD_TOKEN_ABI,
    signer
  );

  const btcTokenContract = new ethers.Contract(
    "0x1bfd67037b42cf73acf2047067bd4f2c47d9bfd6",
    BTC_TOKEN_ABI,
    signer
  );

  const btcBTokenContract = new ethers.Contract(
    "0x7130d2A12B9BCbFAe4f2634d864A1Ee1Ce3Ead9c",
    BTC_TOKEN_ABI,
    signer
  );

  const atomicSwapContract = new ethers.Contract(
    "0xb711f3A71c00D92EF862A4aF2f584635DfE318b8",
    ATOMIC_SWAP_ABI,
    signer
  );
  const forwardContract = new ethers.Contract(
    "0x298259b647d57a8ff6ae82112656b7e30c76601b",
    FORWARD_ABI,
    signer
  );

  const forwardContract_v6 = new ethers.Contract(
    "0x2e45a0a633472e5879b15bf3af78f156909103cc",
    FORWARD_ABI,
    signer
  );

  const usdcTokenContract = new ethers.Contract(
    "0x2791bca1f2de4661ed88a30c99a7a9449aa84174",
    USDC_TOKEN_ABI,
    signer
  );

  if (requestData.op_type === "PERMIT") {
    permit({ tokenContract, requestData, to: requestData.pool, gasPrice });
  } else if (requestData.op_type === "POOL_APPROVE_SYNTH") {
    poolSynthApprove({ tokenContract, requestData, relayerAddress, gasPrice });
  } else if (requestData.op_type === "SYNTH_TRANSFER") {
    synthTransfer({
      tokenContract,
      requestData,
      from: requestData.from,
      to: relayerAddress,
      gasPrice,
    });
  } else if (requestData.op_type === "REDEEM") {
    redeem({ poolContract: poolContractUsdc, requestData, gasPrice });
  } else if (requestData.op_type === "EXECUTE_META_TX") {
    executeMetaTx({
      tokenInstance: busdTokenContract,
      requestData,
      address: relayerAddress,
      gasPrice,
    });
  } else if (requestData.op_type === "POOL_APPROVE_USDC") {
    approveToken({
      tokenInstance: busdTokenContract,
      requestData,
      owner: relayerAddress,
      spender: requestData.pool,
      gasPrice,
    });
  } else if (requestData.op_type === "USDC_TRANSFER") {
    tokenTransfer({
      tokenInstance: busdTokenContract,
      requestData,
      from: requestData.from,
      to: relayerAddress,
      gasPrice,
    });
  } else if (requestData.op_type === "MINT") {
    mint({
      poolContract: poolContractUsdc,
      requestData,
      gasPrice,
    });
  } else if (requestData.op_type === "EXCHANGE") {
    exchange({
      poolContract: poolContractUsdc,
      requestData,
      signer,
      gasPrice,
    });
  } else if (requestData.op_type === "FULL_REDEEM") {
    if (!requestData.redeemGas) {
      throw new Error("Some thing went wrong unable to estimateGas");
    }
    await permit({
      tokenContract,
      requestData,
      to: requestData.pool,
      gasPrice,
    });
    await wait(20000);

    const forwarderNonce = await forwardContract.getNonce(requestData.from);
    console.log(forwarderNonce);
    console.log(
      [
        requestData.from,
        requestData.pool,
        ethers.utils.parseEther("0"),
        parseInt(forwarderNonce.toString()),
        requestData.data,
      ],
      requestData.signature
    );
    const tx2 = await forwardContract.safeExecute(
      [
        requestData.from,
        requestData.pool,
        ethers.utils.parseEther("0"),
        requestData.redeemGas,
        parseInt(forwarderNonce.toString()),
        requestData.data,
      ],
      requestData.signature
    );
    console.log(tx2);
    console.log(await tx2.wait());
  } else if (requestData.op_type === "FULL_MINT") {
    if (!requestData.mintGas) {
      throw new Error("Some thing went wrong unable to estimateGas");
    }
    try {
      const forwarderNonce = await forwardContract.getNonce(requestData.from);
      const tx2 = await forwardContract.safeExecute(
        [
          requestData.from,
          requestData.pool,
          ethers.utils.parseEther("0"),
          requestData.mintGas,
          parseInt(forwarderNonce.toString()),
          requestData.data,
        ],
        requestData.signature
      );
      console.log(tx2);
      console.log(await tx2.wait());
    } catch (error) {
      console.log(
        "**********************************************************************"
      );
      console.log(error);
    }
  } else if (requestData.op_type === "FULL_MINT_V1") {
    await executeMetaTx({
      tokenInstance: usdcTokenContract,
      requestData,
      address: relayerAddress,
      gasPrice,
    });
    await approveToken({
      tokenInstance: usdcTokenContract,
      requestData,
      spender: requestData.pool,
      owner: relayerAddress,
      gasPrice,
    });
    try {
      await tokenTransfer({
        tokenInstance: usdcTokenContract,
        requestData,
        from: requestData.from,
        to: relayerAddress,
        gasPrice,
      });
      await wait(50000);
      await mint({
        poolContract: poolContractUsdc,
        requestData,
        gasPrice,
      });
    } catch (error) {
      console.log(
        "**********************************************************************"
      );
      console.log(error);
      await revertSynthTransfer({
        tokenContract: usdcTokenContract,
        requestData,
        to: requestData.from,
        gasPrice,
      });
    }
  } else if (requestData.op_type === "FULL_EXCHANGE") {
    if (!requestData.exchangeGas) {
      throw new Error("Some thing went wrong unable to estimateGas");
    }
    await permit({
      tokenContract,
      requestData,
      to: requestData.pool,
      gasPrice,
    });

    try {
      const forwarderNonce = await forwardContract.getNonce(requestData.from);
      const tx2 = await forwardContract.safeExecute(
        [
          requestData.from,
          requestData.pool,
          ethers.utils.parseEther("0"),
          requestData.exchangeGas,
          parseInt(forwarderNonce.toString()),
          requestData.data,
        ],
        requestData.signature
      );
      console.log(tx2);
      console.log(await tx2.wait());
    } catch (error) {
      console.log(
        "**********************************************************************"
      );
      console.log(error);
    }
  } else if (requestData.op_type === "FULL_REDEEM_V1") {
    await permit({ tokenContract, requestData, to: relayerAddress, gasPrice });
    await poolSynthApprove({
      tokenContract,
      requestData,
      relayerAddress,
      gasPrice,
    });
    try {
      await synthTransfer({
        tokenContract,
        requestData,
        from: requestData.from,
        to: relayerAddress,
        gasPrice,
      });
      await wait(50000);
      await redeem({ poolContract: poolContractUsdc, requestData, gasPrice });
    } catch (error) {
      console.log(
        "**********************************************************************"
      );
      console.log(error);
      await revertSynthTransfer({
        tokenContract,
        requestData,
        to: requestData.from,
        gasPrice,
      });
    }
  } else if (requestData.op_type === "FULL_EXCHANGE_V1") {
    await permit({ tokenContract, requestData, to: relayerAddress, gasPrice });
    await poolSynthApprove({
      tokenContract,
      requestData,
      relayerAddress,
      gasPrice,
    });
    await synthTransfer({
      tokenContract,
      requestData,
      from: requestData.from,
      to: relayerAddress,
      gasPrice,
    });
    try {
      console.log("Checking the balance");
      const balance = FPN.fromWei(
        await tokenContract.balanceOf(relayerAddress)
      );
      console.log(`${balance.format(4)} Token`, {
        poolContract: poolContractUsdc.address,
      });
      await wait(50000);
      await exchange({
        poolContract: poolContractUsdc,
        requestData,
        signer,
        gasPrice: gasPrice,
      });
    } catch (error) {
      console.log(
        "**********************************************************************"
      );
      console.log(error);
      await revertSynthTransfer({
        tokenContract,
        requestData,
        to: requestData.from,
        gasPrice,
      });
    }
  } else if (requestData.op_type === "ATOMIC_SWAP_MINT") {
    await executeMetaTx({
      tokenInstance: btcTokenContract,
      requestData,
      address: relayerAddress,
      gasPrice,
    });
    await approveToken({
      tokenInstance: btcTokenContract,
      requestData,
      spender: "0xb711f3A71c00D92EF862A4aF2f584635DfE318b8", // Atomic Address
      owner: relayerAddress,
      gasPrice,
    });
    console.log(
      "---------------------------------------------------------------"
    );
    await tokenTransfer({
      tokenInstance: btcTokenContract,
      requestData,
      from: requestData.from,
      to: relayerAddress,
      gasPrice,
    });
    await wait(50000);
    // Transfer from User to relayer
    try {
      const derivative = await poolContractUsdc.getAllDerivatives();
      const tokenAmountIn = requestData.value;
      const collateralAmountOut = new FPN(0).toString();
      const tokenSwapPath = [
        "0x1bfd67037b42cf73acf2047067bd4f2c47d9bfd6", // BTC
        "0x2791bca1f2de4661ed88a30c99a7a9449aa84174", // USDC
      ];
      const synthereumPool = requestData.pool;
      const feePercentage = new FPN(0.001).toString();
      const expiration = requestData.deadline;
      const recipient = requestData.from;
      const numTokens = new FPN(0).toString();
      const minCollateral = new FPN(0).toString();
      console.log("++++++++++++++++++++++++++++++++++++++++++++++++");
      console.log(
        tokenAmountIn,
        collateralAmountOut,
        tokenSwapPath,
        synthereumPool,
        [
          derivative[0],
          numTokens,
          minCollateral,
          feePercentage,
          expiration,
          recipient,
        ]
      );
      const estimateSwapTxGas =
        await atomicSwapContract.estimateGas.swapExactTokensAndMint(
          tokenAmountIn,
          collateralAmountOut,
          tokenSwapPath,
          synthereumPool,
          [
            derivative[0],
            numTokens,
            minCollateral,
            feePercentage,
            expiration,
            recipient,
          ]
        );

      console.log(
        await atomicSwapContract.populateTransaction.swapExactTokensAndMint(
          tokenAmountIn,
          collateralAmountOut,
          tokenSwapPath,
          synthereumPool,
          [
            derivative[0],
            numTokens,
            minCollateral,
            feePercentage,
            expiration,
            recipient,
          ]
        )
      );
      console.log({
        msg: "ATOMIC_SWAP_MINT Estimated Swap Gas Cost",
        gas: estimateSwapTxGas.toString(),
      });

      const swapTx = await atomicSwapContract.swapExactTokensAndMint(
        tokenAmountIn,
        collateralAmountOut,
        tokenSwapPath,
        synthereumPool,
        [
          derivative[0],
          numTokens,
          minCollateral,
          feePercentage,
          expiration,
          recipient,
        ],
        {
          gasPrice: gasPrice,
          gasLimit: estimateSwapTxGas.add(10000),
        }
      );
      console.log(swapTx);
    } catch (error) {
      console.log(
        "**********************************************************************"
      );
      console.log(error);
      await revertSynthTransfer({
        tokenContract: btcTokenContract,
        requestData,
        to: requestData.from,
        gasPrice,
      });
    }
  } else if (requestData.op_type === "FULL_ATOMIC_SWAP_MINT") {
    if (!requestData.mintGas) {
      throw new Error("Some thing went wrong unable to estimateGas");
    }
    await permit({
      tokenContract,
      requestData,
      to: requestData.onChainLiquidationRouter,
      gasPrice,
    });
    //
    const forwarderNonce = await forwardContract.getNonce(requestData.from);
    console.log(forwarderNonce);

    console.log(
      [
        requestData.from,
        requestData.onChainLiquidationRouter,
        ethers.utils.parseEther("0"),
        parseInt(forwarderNonce.toString()),
        requestData.data,
      ],
      requestData.signature
    );
    const tx2 = await forwardContract.safeExecute(
      [
        requestData.from,
        requestData.onChainLiquidationRouter,
        ethers.utils.parseEther("0"),
        requestData.mintGas,
        parseInt(forwarderNonce.toString()),
        requestData.data,
      ],
      requestData.signature
    );
    console.log(tx2);
    console.log(await tx2.wait());
  } else if (requestData.op_type === "ATOMIC_SWAP_REDEEM") {
    await permit({ tokenContract, requestData, to: relayerAddress, gasPrice });

    await poolSynthApprove({
      tokenContract,
      requestData,
      relayerAddress,
      gasPrice,
    });
    // Transfer from User to relayer
    await synthTransfer({
      tokenContract,
      requestData,
      from: requestData.from,
      to: relayerAddress,
      gasPrice,
    });
    try {
      await approveToken({
        tokenInstance: tokenContract,
        requestData,
        spender: "0xb711f3A71c00D92EF862A4aF2f584635DfE318b8", // Atomic Address
        owner: relayerAddress,
        gasPrice,
      });
      await wait(50000);
      const derivative = await poolContractUsdc.getAllDerivatives();
      const numTokens = requestData.value;
      const amountTokenOut = new FPN(0).toString();
      const tokenSwapPath = [
        "0x2791bca1f2de4661ed88a30c99a7a9449aa84174", // USDC
        "0x1bfd67037b42cf73acf2047067bd4f2c47d9bfd6", // BTC
      ];
      const synthereumPool = requestData.pool;
      const feePercentage = new FPN(0.001).toString();
      const expiration = requestData.deadline;
      const recipient = requestData.from;
      const minCollateral = new FPN(0).toString();
      console.log(
        amountTokenOut,
        tokenSwapPath,
        synthereumPool,
        [
          derivative[0],
          numTokens,
          minCollateral,
          feePercentage,
          expiration,
          recipient,
        ],
        recipient
      );
      const estimateSwapTxGas =
        await atomicSwapContract.estimateGas.redeemAndSwapExactTokens(
          amountTokenOut,
          tokenSwapPath,
          synthereumPool,
          [
            derivative[0],
            numTokens,
            minCollateral,
            feePercentage,
            expiration,
            recipient,
          ],
          recipient
        );
      console.log({
        msg: "Estimated Swap Gas Cost",
        gas: estimateSwapTxGas.toString(),
      });
      console.log(
        await atomicSwapContract.populateTransaction.redeemAndSwapExactTokens(
          amountTokenOut,
          tokenSwapPath,
          synthereumPool,
          [
            derivative[0],
            numTokens,
            minCollateral,
            feePercentage,
            expiration,
            recipient,
          ],
          recipient,
          {
            gasPrice: gasPrice,
            gasLimit: estimateSwapTxGas,
          }
        )
      );
      const swapTx = await atomicSwapContract.redeemAndSwapExactTokens(
        amountTokenOut,
        tokenSwapPath,
        synthereumPool,
        [
          derivative[0],
          numTokens,
          minCollateral,
          feePercentage,
          expiration,
          recipient,
        ],
        recipient,
        {
          gasPrice: gasPrice,
          gasLimit: estimateSwapTxGas.add(10000),
        }
      );
      console.log(swapTx);
    } catch (error) {
      console.log(
        "**********************************************************************"
      );
      console.log(error);
      await revertSynthTransfer({
        tokenContract,
        requestData,
        to: requestData.from,
        gasPrice,
      });
    }
  } else if (requestData.op_type === "FULL_ATOMIC_SWAP_REDEEM") {
    if (!requestData.redeemGas) {
      throw new Error("Some thing went wrong unable to estimateGas");
    }
    await permit({
      tokenContract,
      requestData,
      to: requestData.onChainLiquidationRouter,
      gasPrice,
    });
    //
    const forwarderNonce = await forwardContract.getNonce(requestData.from);
    console.log(forwarderNonce);

    console.log(
      [
        requestData.from,
        requestData.onChainLiquidationRouter,
        ethers.utils.parseEther("0"),
        parseInt(forwarderNonce.toString()),
        requestData.data,
      ],
      requestData.signature
    );
    const tx2 = await forwardContract.safeExecute(
      [
        requestData.from,
        requestData.onChainLiquidationRouter,
        ethers.utils.parseEther("0"),
        requestData.redeemGas,
        parseInt(forwarderNonce.toString()),
        requestData.data,
      ],
      requestData.signature
    );
    console.log(tx2);
    console.log(await tx2.wait());
  } else if (requestData.op_type === "BTC_FULL_SEND") {
    await executeMetaTx({
      tokenInstance: btcTokenContract,
      requestData,
      address: relayerAddress,
      gasPrice,
    });
    await tokenTransfer({
      tokenInstance: btcTokenContract,
      requestData,
      from: requestData.from,
      to: requestData.recipient,
      gasPrice,
    });
  } else if (requestData.op_type === "FULL_SEND") {
    await permit({ tokenContract, requestData, to: relayerAddress, gasPrice });
    await synthTransfer({
      tokenContract,
      requestData,
      from: requestData.from,
      to: requestData.recipient,
      gasPrice,
    });
  } else if (requestData.op_type === "USDC_FULL_SEND") {
    await executeMetaTx({
      tokenInstance: usdcTokenContract,
      requestData,
      address: relayerAddress,
      gasPrice,
    });
    await tokenTransfer({
      tokenInstance: usdcTokenContract,
      requestData,
      from: requestData.from,
      to: requestData.recipient,
      gasPrice,
    });
  } else if (requestData.op_type === "BTCB_FULL_SEND") {
    await tokenTransfer({
      tokenInstance: btcBTokenContract,
      requestData,
      from: requestData.from,
      to: requestData.recipient,
      gasPrice,
    });
  } else if (requestData.op_type === "FULL_REDEEM_V6") {
    if (!requestData.redeemGas) {
      throw new Error("Some thing went wrong unable to estimateGas");
    }
    await permit({
      tokenContract,
      requestData,
      to: requestData.pool,
      gasPrice,
    });
    //    await wait(40000);

    const forwarderNonce = await forwardContract_v6.getNonce(requestData.from);
    console.log(forwarderNonce);
    console.log(
      [
        requestData.from,
        requestData.pool,
        ethers.utils.parseEther("0"),
        parseInt(forwarderNonce.toString()),
        requestData.data,
      ],
      requestData.signature
    );
    const tx2 = await forwardContract_v6.safeExecute(
      [
        requestData.from,
        requestData.pool,
        ethers.utils.parseEther("0"),
        requestData.redeemGas,
        parseInt(forwarderNonce.toString()),
        requestData.data,
      ],
      requestData.signature
    );
    console.log(tx2);
    console.log(await tx2.wait());
  } else if (requestData.op_type === "FULL_MINT_V6") {
    if (!requestData.mintGas) {
      throw new Error("Some thing went wrong unable to estimateGas");
    }

    await permit({
      tokenContract,
      requestData,
      to: requestData.pool,
      gasPrice,
    });

    try {
      const forwarderNonce = await forwardContract_v6.getNonce(
        requestData.from
      );
      const tx2 = await forwardContract_v6.safeExecute(
        [
          requestData.from,
          requestData.pool,
          ethers.utils.parseEther("0"),
          requestData.mintGas,
          parseInt(forwarderNonce.toString()),
          requestData.data,
        ],
        requestData.signature
      );
      console.log(tx2);
      console.log(await tx2.wait());
    } catch (error) {
      console.log(
        "**********************************************************************"
      );
      console.log(error);
    }
  } else if (requestData.op_type === "USDC_FULL_SEND_V6") {
    await permit({ tokenContract, requestData, to: relayerAddress, gasPrice });
    await synthTransfer({
      tokenContract,
      requestData,
      from: requestData.from,
      to: requestData.recipient,
      gasPrice,
    });
  }
}

// Sample typescript type definitions
type EnvInfo = {
  API_KEY: string;
  API_SECRET: string;
};

// To run locally (this code will not be executed in Autotasks)
if (require.main === module) {
  require("dotenv").config();
  const { API_KEY: apiKey, API_SECRET: apiSecret } = process.env as EnvInfo;
  handler({
    apiKey,
    apiSecret,
    request: {
      body: {
        spender: "0x4c28a9856211dde89f910c4d4b5912a7a808a6e5",
        deadline: 1641726705068,
        to: "0x4e3Decbb3645551B8A19f0eA1678079FCB33fB4c",
        from: "0x61b5a06ce0fcda6445fb454244ce84ed64c41aca",
        allowance_Value: "1000000000000000000000000000000",
        value: "15000000000000000000",
        op_type: "LOG",
        pool: "0xcbba8c0645ffb8aa6ec868f6f5858f2b0eae34da",
        r: "0x4770c57060eb3b06667bab3e123469d87fafd47321b0c7b8a501245b2e9e4617",
        s: "0x07f8b09d3fc68cd5971501ca7a250ca6f8536dee6ddf9a34afbc87afe0e3663d",
        v: 28,

        // owner: "0x61b5a06ce0fcda6445fb454244ce84ed64c41aca",
        // spender: "0x59a266d00d20b0edf7a8f12f468ce7211d8f0de2",
        // from: "0x61b5a06ce0fcda6445fb454244ce84ed64c41aca",
        // allowance_Value: new FPN(1000000000000).toString(),
        // value: new FPN(10).toString(),
        // deadline: 1668765976,
        // op_type: "REDEEM",
        // to: "0x02CBE6055F8aad745321f70d6aDD4711455c7F45",
        // pool: "0x06ED7844293F6433791A22D426Fb7Ad782Ed82f1",
        // r: "0xa4ce705a6050218ab48f8f4212641e766263c7a18cfd831c7dafd7eb2198fd7d",
        // s: "0x3b78bae3df2b9fb3102a12a03760eeaaaa28bf7cf48777f53a72380a4176486f",
        // v: 28,
      },
    },
  })
    .then(() => process.exit(0))
    .catch((error: Error) => {
      console.error(error);
      process.exit(1);
    });
}
